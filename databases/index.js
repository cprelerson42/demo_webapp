'use strict';
console.log("hi")
/* eslint-disable */


const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const { MongoClient } = require('mongodb');
const redis = require('redis');
const conf = require('./config.json');
var random_string = require('randomstring')
const { Client } = require('pg')


const server = restify.createServer({'name':'dbms'});
server.pre(restify.pre.dedupeSlashes());
server.use(restify.plugins.acceptParser(server.acceptable));

server.use(restify.plugins.bodyParser({ mapParams: false }));

const cors = corsMiddleware({
  origins: ['*'],
//  allowHeaders: conf.cors.allowHeaders,
});
server.pre(cors.preflight);
server.pre(cors.actual);

var mongo_url = "mongodb://" + conf.mongo.username + ":" + conf.mongo.password + "@" + conf.mongo.url + ":" + conf.mongo.port + "/"
var web_db = conf.mongo.database
var dbo

MongoClient.connect(mongo_url, function(err, db) {
  if (err) throw err;
  console.log("Database created!");
  dbo = db.db(web_db)
  var item = { "name": "prod1", "type": "widget1", "value": "1.20"}
  dbo.collection(conf.mongo.collection).drop()
  dbo.collection(conf.mongo.collection).insertOne(item, function(err, res){
    if (err) throw err;
    console.log("1 document inserted");
  })
});

var redis_client = redis.createClient(conf.redis.port, conf.redis.url)
redis_client.on('connect', (err)=>{console.log("Connected to redis db" + err)})

server.get('/mongo',
  (req, res, next) => {
    
    var result_obj = {}
    
    dbo.collection("catalog").find({}).toArray(function(err, result) {
      result_obj['mongo'] = result
      res.send(result_obj)
    })

    console.log("Sending data ")
    console.log(result_obj)
    next();
  });

server.get('/redis',
  (req, res, next) => {
    
    var result_obj = {}


    redis_client.set('connection_string', random_string.generate(), redis.print);
    redis_client.get('connection_string', function (error, result) {
      if (error) {
        console.log(error);
        throw error;
      }
      console.log('GET result ->' + result);
      result_obj['redis'] = result
      res.send(result_obj)
    });

    
    console.log("Sending data ")
    console.log(result_obj)
    next();
  });
  
  
server.get('/psql',
  (req, res, next) => {
    const client = new Client({
      user: conf.postgres.user,
      password: conf.postgres.password,
      host: conf.postgres.url,
      port: conf.postgres.port,
      database: conf.postgres.database
    })
    var result_obj = {}
    client.connect()
    client.query('SELECT * FROM ' + conf.postgres.table, (err, result) => {
      result_obj['postgres'] = result.rows
      res.send(result_obj)
      client.end()
    }
  )
    next();
  });
  
  

  
  
function startRestifyServer() {
  server.listen(12345, "0.0.0.0", () => {
    console.log(`Listening for connections at ${server.url}`);
  });
}

startRestifyServer();
